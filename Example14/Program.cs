﻿using System;

class Example
{
    static void Main()
    {
        double s1 = 4.0;
        double s2 = 5.0;

        //Dynamically initialize hypot

        double hypot = Math.Sqrt(s1 * s1 + s2 * s2);

        Console.WriteLine("Hypotenuse of triangle with sides s1 and s2 is {0:#.###} ",hypot);
    }
}